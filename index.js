'use strict';

const fs = require('fs');

const SpeakSoftly = require('speak-softly'),
	xsd = require('libxml-xsd');

const logger = new SpeakSoftly();

function getSchema (xsdPath) {
	return new Promise((resolve, reject) => xsd.parseFile(xsdPath, (err, schema) => err ? reject(err) : resolve(schema)));
}

function validateXml (schema, xmlPath) {
	return new Promise((resolve, reject) => {
			fs.readFile(xmlPath, 'utf-8', (err, data) => err ? reject(err) : resolve(data));
		})
		.then(xmlString => {
			return new Promise((resolve, reject) => {
				schema.validate(xmlString, (err, errors) => err ? reject(err) : resolve(errors || []));
			});
		});
}

module.exports = function validate(schema, files) {
	return getSchema(schema)
		.catch(err => {
			logger.caption('Encountered an error reading the schema:');
			logger.error(err.stack);
			logger.break();
			process.exit();
		})
		.then(schema => Promise.all(files.map(file => validateXml(schema, file)
			.catch(err => {
				return [
					err
				];
			})
			.then(errors => {
				logger.caption(file);

				if (errors.length)
					logger.list(errors.map(error => error.message.trim()));
				else
					logger.debug('No errors');
			}))))
		.then(() => {
			logger.break();
		})
		.catch(err => {
			logger.caption('Encountered an unknown error:');
			logger.error(err.stack);
			logger.break();
			process.exit();
		});
};
